<?php
if(is_shop())
{
?>
<div class="page-content">
  <div class="page-wrapper">
    <div class="container">
      <section class="brands-product-loop">
        <h1 class="woo-title"><?php the_title(); ?></h1>
        <div class="tudor-btn-wrap tc">
          <a href="<?= get_permalink( 2153 ); ?>" class="btn">Tudor History</a>
        </div>
        <?php echo do_shortcode('[products]'); ?>
      </section>
    </div>
  </div>
</div>
<?php
} elseif(is_product_category() || is_product_tag())
{
  ?>
  <div class="page-content">
    <div class="page-wrapper">
      <div class="container">
        <section class="brands-product-loop">
          <h1 class="woo-title"><?php the_title(); ?> <?= get_field('subtitle'); ?></h1>
          <div class="tudor-btn-wrap tc">
            <a href="<?= get_permalink( 2153 ); ?>" class="btn">Tudor History</a>
          </div>
          
          <?php the_content(); ?>
        </section>
      </div>
    </div>
  </div>
  <?php
} else {
  get_template_part('templates/flexible-content/whisk-fx-init');
}
?>
