<?php global $query; ?>
<?php if (!have_posts()) : ?>
  <div class="alert alert-warning container">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<div class="page-content">
  <div class="page-wrapper">
    <div id="append-post" class="clearfix">
      <div class="append-post"></div>
    </div>
    <div class="posts-wrapper">
      <div class="paging-content">
        <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>
      </div>
    </div>
    <nav id="paging" class="clear">
      <div class="nav-previous">
        <?php
        $next = get_next_posts_link( 'Load More <span class="css-loader"></span>', $query->max_num_pages );

        echo $next;
        ?>
      </div>
    </nav>
  </div>
</div>
