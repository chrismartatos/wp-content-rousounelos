<?php

/*
****** Loop
*/
add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

function woocommerce_template_loop_product_title()
{
  if(!is_singular('product')){
    $terms = wp_get_post_terms(get_the_ID(), array('product_cat'),  array("fields" => "names"));

    if(!empty($terms)){
      echo '<span class="term">'.$terms[0].'</span>';
    }


    echo '<h2 class="woocommerce-loop-product__title">'.get_the_title().'</h2>';
    echo '<div class="desc">'.get_the_excerpt().'</div>';
  }
}



/*-------------------------------------------------------------------------------------
      loop
-------------------------------------------------------------------------------------*/
add_action( 'woocommerce_before_shop_loop_item_title', 'loop_product_thumb_open', 5, 2);
add_action( 'woocommerce_before_shop_loop_item_title', 'loop_product_thumb_close', 12, 2);
add_action( 'woocommerce_before_subcategory_title', 'loop_product_thumb_open', 5, 2);
add_action( 'woocommerce_before_subcategory_title', 'loop_product_thumb_close', 12, 2);
/**
* Add wrap around all product images.
*
*/
function loop_product_thumb_open($post)
{
    global $post;

    $product = wc_get_product( $post->ID );
    $attachment_ids = $product->get_gallery_image_ids();

    if(!empty($attachment_ids))
    {
      $class = (!empty($attachment_ids[0]))? 'hover-image': 'hover-empty';
      $image_src = wp_get_attachment_image_src($attachment_ids[1],'shop_catalog',false,'data-hover');

      echo '<div class="clearfix product-thumb-hover-wrap '.$class.'" data-id="'.$attachment_ids[1].'">';
      echo '<div class="attachment-shop_catalog" style="background-image: url('.$image_src[0].');"></div>';
    }

}

function loop_product_thumb_close()
{
  global $post;

  $product = wc_get_product( $post->ID );
  $attachment_ids = $product->get_gallery_image_ids();

  if(!empty($attachment_ids))
  {
    echo '</div>';
  }

}
