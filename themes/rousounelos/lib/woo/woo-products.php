<?php
/*-------------------------------------------------------------------
  Breadcrumb
-------------------------------------------------------------------*/
add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );

function woo_custom_breadrumb_home_url() {
  $products = get_home_url().'/our-brands';

  return $products;
}

//Shop
add_filter( 'woocommerce_breadcrumb_defaults', 'woo_change_breadcrumb_home_text' );

function woo_change_breadcrumb_home_text( $defaults ) {
	$defaults['home'] = 'Brands';

	return $defaults;
}
