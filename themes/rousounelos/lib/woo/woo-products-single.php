<?php

 /*-------------------------------------------------------------------
    Single product - Remove related products output
 -------------------------------------------------------------------*/
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/*-------------------------------------------------------------------
   Single product
-------------------------------------------------------------------*/
add_action( 'woocommerce_before_add_to_cart_form', 'single_product_shipping_info');

function single_product_shipping_info()
{
  $elta_msg = '<div class="taken-cart-msg">test</div>';

  echo $elta_msg;
}


/*-------------------------------------------------------------------
  Remove elements
-------------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

// Remove the product description Title
add_filter( 'woocommerce_product_description_heading', '__return_null' );


add_filter( 'woocommerce_get_price_html', 'njengah_text_after_price' );

function njengah_text_after_price($price)
{
  global $product;
  global $woocommerce_loop;

  $get_terms = wp_get_post_terms(get_the_ID(), array('product_cat'),  array("fields" => "names"));

  $text_before_price1 = '';
  $text_before_price2 = '';
  $text_after_price = '';

  //Upsells
  if ( is_product() && $woocommerce_loop['name'] == 'up-sells' || is_product() && $woocommerce_loop['name'] == 'related')
  {
    if(!empty($get_terms)){
      $text_before_price2 = '<span class="term">'.$get_terms[0].'</span>';
    } else {
      $text_before_price2 = '';
    }

    $price = '';

    $text_after_price  = ' <h3 class="woocommerce-loop-product__title">'.get_the_title().'</h3> ';
  }
  elseif(is_singular('product'))
  {
    //Product summary
    $get_sku = $product->get_sku();

    if(!empty($get_sku)){
      $text_before_price1 = '<div class="sku-right"><span>SKU:</span> '.$get_sku.'</div>';
    } else {
      $text_before_price1 = '';
    }

    if(!empty($get_terms)){
      $text_before_price2 = '<span class="term">'.$get_terms[0].'</span>';
    } else {
      $text_before_price2 = '';
    }

    $text_after_price  = ' <h1>'.get_the_title().'</h1> ';
  } else {
    $price = '';
  }

  return  $text_before_price1 . $text_before_price2 . $price .   $text_after_price;
}


/*-------------------------------------------------------------------
  Add desc under price and custom fields
-------------------------------------------------------------------*/
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );

function woocommerce_template_product_description()
{
  $field_case = get_field('case');
  $field_bezel = get_field('bezel');
  $field_movement = get_field('movement');
  $field_dial = get_field('dial');
  $field_winding_crown = get_field('winding_crown');
  $field_crystal = get_field('crystal');
  $field_waterproofness = get_field('waterproofness');
  $field_bracelet = get_field('bracelet');

  echo '<a href="#product-inquiries" class="btn scroll-to" data-offset="40">Make an enquiry</a>';

  if(!empty($field_case)):
    echo '<article class="item-info">';
    echo '<h3>Case</h3>';
    echo '<p>'.$field_case.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_movement)):
    echo '<article class="item-info">';
    echo '<h3>Movement Type</h3>';
    echo '<p>'.$field_movement.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_dial)):
    echo '<article class="item-info">';
    echo '<h3>Glass</h3>';
    echo '<p>'.$field_dial.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_crystal)):
    echo '<article class="item-info">';
    echo '<h3>Crystal</h3>';
    echo '<p>'.$field_crystal.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_waterproofness)):
    echo '<article class="item-info">';
    echo '<h3>Water Resistance</h3>';
    echo '<p>'.$field_waterproofness.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_bracelet)):
    echo '<article class="item-info">';
    echo '<h3>Strap</h3>';
    echo '<p>'.$field_bracelet.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_bezel)):
    echo '<article class="item-info">';
    echo '<h3>Bezel</h3>';
    echo '<p>'.$field_bezel.'</p>';
    echo '</article>';
  endif;

  if(!empty($field_winding_crown)):
    echo '<article class="item-info">';
    echo '<h3>Winding crown</h3>';
    echo '<p>'.$field_winding_crown.'</p>';
    echo '</article>';
  endif;

  wc_get_template( 'single-product/tabs/description.php' );
}




/*-------------------------------------------------------------------
  single products
-------------------------------------------------------------------*/
add_action( 'woocommerce_before_single_product', 'my_product_before_single' );
add_action( 'woocommerce_after_single_product', 'my_product_after_single' );

function my_product_before_single() {
  echo '<div class="container">';
}

function my_product_after_single() {
  echo '</div>';
}


//single summary
add_action( 'woocommerce_before_single_product_summary', 'my_product_before_summary' );
add_action( 'woocommerce_after_single_product_summary', 'my_product_after_summary' );

function my_product_before_summary() {
  echo '<div class="clearfix">';
}

function my_product_after_summary() {
  echo '</div>';
}


remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

/*------------------------------------------------------------
  Image slideshow thumbnails
-------------------------------------------------------------*/
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size )
{
  return array(
    'width' => 220,
    'height' => 220,
    'crop' => 0,
  );
});


add_filter( 'woocommerce_single_product_image_thumbnail_html', 'rm_woocommerce_remove_featured_image', 10, 2 );

/**
 * Exclude the featured image from appearing in the product gallery, if there's a product gallery.
 *
 * @param array $html Array of HTML to output for the product gallery.
 * @param array $attachment_id ID of each image variables.
 */
 function rm_woocommerce_remove_featured_image( $html, $attachment_id )
 {
 	global $post, $product;
 	// Get the IDs.
 	$attachment_ids = $product->get_gallery_image_ids();

 	// Look for the featured image.
 	$featured_image = get_post_thumbnail_id($post->ID);


 	// If there is one, exclude it from the gallery.
 	if ( is_product() && $attachment_id === $featured_image ) {
 		$html = '';
 	}
 	return $html;
 }
