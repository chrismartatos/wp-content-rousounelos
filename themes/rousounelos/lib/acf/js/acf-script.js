jQuery(document).ready(function()
{
  jQuery(window).load(function()
  {
    //Target layout option
    var $layout = jQuery('.acf-field[data-name="choose_page_layout"]');

    //if exist
    if($layout.length)
    {
      $layout.find("ul li").each(function(i)
      {
        jQuery("input",this).after('<div class="acf-layout layout-'+i+'"><span class="border-one"></span></div>');
      });
    }
  });
});
