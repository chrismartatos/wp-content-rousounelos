<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */

function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */

function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Move Gravity Forms JS to footer
 */

 function move_gform_js() {
   return '0';
 }

add_filter( 'gform_init_scripts_footer',  __NAMESPACE__ . '\\move_gform_js' );

/**
 * Use Site Icon on Login pages
 */

function my_login_logo() { ?>
    <style type="text/css">
        body.login {
          background: #fff;
          padding-top: 8%;
          overflow: hidden;
        }
        #login {
          border: 1px solid #d1d1d1;
          padding: 15px!important;
        }
        #login h1 {
          margin-top: 30px;
        }
        #login h1 a, .login h1 a {
          background-image: url(<?php site_icon_url(); ?>);
          padding-bottom: 30px;
        }
        #login form {
          -webkit-box-shadow: none;
          box-shadow: none;
        }
        .login #nav a:hover,
        .login #backtoblog a:hover,
        .login h1 a:hover {
          color: #4f4f4f!important;
        }
        input[type="text"]:focus {
          border-color: #828282!important;
          -webkit-box-shadow: 0 0 2px #828282!important;
          box-shadow: 0 0 2px #828282!important;
        }
        input[type="password"]:focus {
          border-color: #828282!important;
          -webkit-box-shadow: 0 0 2px #828282!important;
          box-shadow: 0 0 2px #828282!important;
        }
        input[type="checkbox"]:checked:before {
          color: #4f4f4f!important;
        }
        .wp-core-ui .button-primary {
          background: #828282!important;
          border-color: #828282!important;
          border-radius: 0!important;
          -webkit-box-shadow: 0 1px 0 #828282!important;
          box-shadow: 0 1px 0 #828282!important;
          color: #fff;
          text-decoration: none;
          text-shadow: none!important;
          transition: all ease-in-out .3s;
        }
        .wp-core-ui .button:hover {
          background: #4f4f4f!important;
          border-color: #4f4f4f!important;
          -webkit-box-shadow: 0 1px 0 #4f4f4f!important;
          box-shadow: 0 1px 0 #4f4f4f!important;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\my_login_logo', 100 );

/**
 * Use Site Address on Login logo
 */

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', __NAMESPACE__ . '\\my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Your Site Name and Info';
}
add_filter( 'login_headertitle', __NAMESPACE__ . '\\my_login_logo_url_title' );

/**
 * Remove WordPress logo from Admin page
 */

function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render',  __NAMESPACE__ . '\\annointed_admin_bar_remove', 0);
