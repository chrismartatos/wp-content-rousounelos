<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',     // Scripts and stylesheets
  'lib/extras.php',     // Custom functions
  'lib/setup.php',      // Theme setup
  'lib/titles.php',     // Page titles
  'lib/wrapper.php',    // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/acf/acf-functions.php', // ACF init
  'lib/brands/register-brand-cpt.php',
  'lib/woo/woo-products.php',
  'lib/woo/woo-products-loop.php',
  'lib/woo/woo-products-single.php',
];

foreach ($sage_includes as $file)
{
  if (!$filepath = locate_template($file))
  {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);




 /*-------------------------------------------------------------------------------------
        Enqueue Google fonts
 -------------------------------------------------------------------------------------*/
 add_action( 'wp_enqueue_scripts', 'google_fonts' );

 function google_fonts()
 {
   wp_enqueue_style( 'rousounelos-fonts', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Roboto:100,300,400,500,700,900', false );
 }

/*----------------------------------------------------------------------------------
	Contact form 7 - disable p tag
-----------------------------------------------------------------------------------*/
//define ('WPCF7_AUTOP', false);


/*-----------------------------------------------------------------------------------
    SVG
-----------------------------------------------------------------------------------*/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*-----------------------------------------------------------------------------------
    ACF: Register Google Maps API KEY for acf
-----------------------------------------------------------------------------------*/
add_action('acf/init', 'my_acf_init');

function my_acf_init()
{
	acf_update_setting('google_api_key', 'AIzaSyAM_yb1wCAnB6kenjT-gR9f7NkFMROFJcw');
}

function register_scripts()
{
  wp_enqueue_script( 'googlemap-api' , 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAM_yb1wCAnB6kenjT-gR9f7NkFMROFJcw', array('jquery'), null, true );
}

add_action( 'wp_enqueue_scripts', 'register_scripts' );


/*-------------------------------------------------------------------------------------
       Register scripts
-------------------------------------------------------------------------------------*/
add_action( 'wp_enqueue_scripts', 'register_scripts_styles' );

function register_scripts_styles()
{
  wp_enqueue_script( 'history' , get_stylesheet_directory_uri() . '/assets/scripts/history.min.js', ['jquery'], null, true );
}


/*----------------------------------------------------------
		ELEMENTS: Social
----------------------------------------------------------*/
function share_this($atts) {

	ob_start();

	include('templates/share-page.php');

	$content = ob_get_clean();

	return $content;
}


add_shortcode('share', 'share_this');
