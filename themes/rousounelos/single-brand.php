<?php
  get_template_part('templates/flexible-content/whisk-fx-init');
?>

<?php
global $post;
global $post_type;

  $back_href = "/our-brands";

  //Prev next
	$prevPost = get_previous_post();
	$nextPost = get_next_post();

	$parents = get_post_ancestors( $post->ID );

	$prevTitle = get_the_title($prevPost->ID);
	$prevLink = get_permalink($prevPost->ID);
  $prevThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($prevPost->ID,'full'));


	$nextTitle = get_the_title($nextPost->ID);
	$nextLink = get_permalink($nextPost->ID);
  $nextThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($nextPost->ID,'full'));

  $first_item = get_field('brands_first_item','option');
  $last_item = get_field('brands_last_item','option');

  $ID = get_the_id();
?>
<div class="container">
  <div class="row">

    <nav id="post-nav" class="call-to-action">
      <?php if($first_item==$ID): ?>
        <a class="first-item-active prev-post arrows item js-redirect" href="<?= get_the_permalink($last_item); ?>" data-title="<?= get_the_title($last_item); ?>">
          <h4><span class="ent-arr">&larr;</span> <?= get_the_title($last_item); ?></h4>
        </a>
      <?php else: ?>
      <a class="prev-post item js-redirect" href="<?= $prevLink; ?>" data-title="<?= $prevTitle; ?>">
        <h4><span class="ent-arr">&larr;</span> <?= $prevTitle; ?></h4>
      </a>
      <?php endif; ?>

      <a class="all item js-redirect" href="<?= get_home_url(); ?><?= $back_href; ?>">View all <span>the brands</span></a>

      <?php if($last_item==$ID): ?>
        <a class="last-item-active next-post arrows item js-redirect" href="<?= get_the_permalink($first_item); ?>" data-title="<?= get_the_title($first_item); ?>">
          <h4><?= get_the_title($first_item); ?> <span class="ent-arr">&rarr;</span></h4>
        </a>
      <?php else: ?>
        <a class="next-post arrows item js-redirect" href="<?= $nextLink; ?>" data-title="<?= $nextTitle; ?>">
          <h4><?= $nextTitle; ?> <span class="ent-arr">&rarr;</span></h4>
        </a>
      <?php endif; ?>
    </nav>

  </div>
</div>
