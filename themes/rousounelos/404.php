<?php use Roots\Sage\Titles; ?>

<div class="page-header bg-cover bg-fixed" style="background-image:url(<?php echo get_home_url(); ?>/wp-content/uploads/2017/03/ROUSOUNELOS-ABOUT-US_01.jpg);">
  <div class="vcenter-outer">
    <div class="vcenter-inner">

      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-2"></div>
          <div class="col-sm-12 col-md-8">
            <h1 class="page-title">404 Error</h1>
          </div>
          <div class="col-sm-12 col-md-2"></div>
        </div>
      </div><!--END .container-->

    </div><!--END .vcenter-inner-->
  </div><!--END .vcenter-outer-->
</div>

<div class="page-content">
  <div class="page-wrapper">
    <article class="text-editor alert alert-warning">
      <p><?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?></p>
      <nav class="footer-nav cul">
        <ul>
          <li><a href="<?php echo get_permalink(get_page_by_path('about-us')); ?>"> About Us </a> • </li>
          <li><a href="<?php echo get_permalink(get_page_by_path('our-brands')); ?>"> Our Brands </a> • </li>
          <li><a href="<?php echo get_permalink(get_page_by_path('news')); ?>"> News </a> • </li>
          <li><a href="<?php echo get_permalink(get_page_by_path('contact-us')); ?>"> Contact</a></li>
        </ul>
      </nav>
      <?php //get_search_form(); ?>
    </article>
  </div>
</div>
