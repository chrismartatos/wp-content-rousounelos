/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function()
      {
        var $body = $("body");
        var $form_field = $('.wpcf7-form input[name="text-745"]');


        /*  FN: Preloader
        ------------------------------------------------------------------*/
        $body.addClass("loading");

        $(window).load(function()
        {
          setTimeout(function()
          {
            $("#preloader").animate({opacity:0},720).promise().done(function()
            {
              $body.addClass("animations-init");
            });

            //Single product
            if($('body.single-product').length)
            {
              var $subject = $('.wpcf7-form input[name="text-677"]');

              //Click second thumnb
              $('.flex-control-nav li:eq(1)').find('img').click();
              $('.flex-control-nav li:eq(1)').click();

              //Email
              $subject.val($('.summary h1').text());
            }
          },700);


          /*  FN: Home
          --------------------------------------------------------*/
          $("#home-slideshow").headerSlideshow();
          $("#rolex-slideshow").rolexSlideshow();

          /*  FN: URL
          --------------------------------------------------------*/
          if($form_field.length)
          {
            $form_field.val(window.location.href);
          }
        });

        /*  FN: Header JS
        ------------------------------------------------------------------*/
        $("#main-header").headerFN();

        /*  FN: Mobile nav
        ------------------------------------------------------------------*/
        $("#mobile-nav-container").mobileNav();

        /*  FN: Gallery
        ------------------------------------------------------------------*/
        $(".fx-gallery").gallerySlideshow();

        /*  FN: Ajax Load More
        ------------------------------------------------------------------*/
        $("#paging").ajaxLoadMore();

        /*  FN: Ajax post
        ------------------------------------------------------------------*/
        $(".ajax-post:not(.active)").getAjaxPost();

        /*  FN: Share
        ------------------------------------------------------------------*/
        $(".share-buttons .anchor-hover").shareThis();

        /*  FN: Slideshow News
        ------------------------------------------------------------------*/
        $("#post-slideshow").postSlideshow();

        /*  FN: Brands
        ------------------------------------------------------------------*/
        $(".brand-alpha").brandAlpha();

        /* FN: Scroll to fn
        ----------------------------------------------------*/
        $('.scroll-to').scrollToSection();

        /* FN: Hover Intent
        ----------------------------------------------------*/
        $("#rolex-navigation ul > li.menu-item-has-children").hoverIntent({
  			    sensitivity: 1,
  			    interval: 10,
  			    timeout: 100,
  			    over:function(){
  			        $(".sub-menu:first",this)
  			            .removeClass("hoverOut").toggleClass("hoverIn").fadeIn();
  			    },
  			    out: function(){
  			        $(".sub-menu:first",this)
  			            .removeClass("hoverIn").toggleClass("hoverOut").fadeOut();
  			    }
  			});

        $('#menu-mobile-nav .menu-item-has-children').find('a:first').after('<span class="toggle-menu-item toggle"></span>').promise().done(function()
        {
          $('#menu-mobile-nav .toggle-menu-item').click(function()
          {
            var $dropdown = $(this).parents('.menu-item-has-children').eq(0);

            if($(this).hasClass('active'))
            {
              $(this).removeClass('active');
              $dropdown.find('.sub-menu').slideUp();
            } else {
              $(this).addClass('active');
              $dropdown.find('.sub-menu').slideDown();
            }
          });
        });


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired

        /*  FN: Map
        ------------------------------------------------------------------*/
        $("#map-init").showMap();

        /*  FN: Page
        ------------------------------------------------------------------*/
        $('.home .secondary a,#menu-main-nav a,.js-redirect:not([href^="#"],[href*="wp-login"],[href*="wp-admin"])').pageTransition();
      }
    },
    // Home page
    'contact_us': {
      init: function() {
        // JavaScript to be fired on the home page
        $("#map").googleMap();
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);



/*------------------------------------------------------------------------------------
    Page redirect
-------------------------------------------------------------------------------------*/
$.fn.pageTransition = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.each(function()
    {
      $(this).on('click',function(e)
      {
        e.preventDefault();

        //Link
        var _href = $(this).attr("href");

        //Transitions init
        $("body").addClass("page-transition");

        //Redirect
        setTimeout(function()
        {
          window.location.href = _href;
        },1040);

        //Top
        $("body,html").animate({
          scrollTop: 0
        },400);

        return false;
      });
    });
  }
};




/*------------------------------------------------------------------------------------
    Scroll to Section
-------------------------------------------------------------------------------------*/
$.fn.scrollToSection = function(_offset)
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $scroll = ($(this).attr('href'))? $(this).attr('href') : $(this).attr('data-id');
      var $targetScroll = $($scroll);

      if($targetScroll.length)
      {
        var _offset_top = ($(this).attr('data-offset'))?parseInt($(this).attr('data-offset')):0;
        var _pos = $targetScroll.offset().top-_offset_top;

        $('html, body').animate({
          scrollTop:_pos
        }, 900);
      }
      else
      {
        alert('Section not found');
      }
    });

  }
};


/*------------------------------------------------------------------------------------
    Post slideshow
-------------------------------------------------------------------------------------*/
$.fn.postSlideshow = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.each(function()
    {
      $(this).slick({
    		autoplay: true,
    		slide:'.slide-item',
    		autoplaySpeed: 6000,
        speed:900,
    		arrows:true,
    		dots:true,
        fade:true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        rows:0,
        lazyLoad:'ondemand',
    		infinite:false,
    		slidesToShow:1,
    		slidesToScroll:1,
        customPaging:function(slider, i){
          return '<span class="dot"> </span>';
        }
  		});
    });
  }
};


/*------------------------------------------------------------------------------------
    Brand Alpha
-------------------------------------------------------------------------------------*/

$.fn.brandAlpha = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $gallery_one   = $(".gallery-1").find(".brand-slideshow");
    var $gallery_two   = $(".gallery-2").find(".brand-slideshow");
    var $gallery_three = $(".gallery-3").find(".brand-slideshow");

    if($gallery_one.length)
    {
      var _speed1 = $gallery_one.attr("data-speed");

      $gallery_one.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:_speed1,
        speed:1000,
    		arrows:true,
    		dots:false,
        fade:true,
        rows:0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-1"),
        nextArrow:$(".next-1")
  		});
    }

    if($gallery_two.length)
    {
      var _speed2 = $gallery_two.attr("data-speed");

      $gallery_two.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:_speed2,
        speed:1000,
    		arrows:true,
    		dots:false,
        fade:true,
        rows:0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-2"),
        nextArrow:$(".next-2")
  		});
    }

    if($gallery_three.length)
    {
      var _speed3 = $gallery_three.attr("data-speed");

      $gallery_three.slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:_speed3,
        speed:1000,
    		arrows:true,
    		dots:false,
        fade:true,
        rows:0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev-3"),
        nextArrow:$(".next-3")
  		});
    }
  }
};

/*------------------------------------------------------------------------------------
    Share this
-------------------------------------------------------------------------------------*/
$.fn.shareThis = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $wrapper = $(this).parents(".share-buttons");

      $wrapper.toggleClass("active");
    });
  }
};


/*------------------------------------------------------------------------------------
    Gallery
-------------------------------------------------------------------------------------*/
$.fn.gallerySlideshow = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.each(function()
    {
      var _speed = $(this).attr("data-speed");

      $(this).slick({
    		autoplay:true,
    		slide:".slide-item",
    		autoplaySpeed:_speed,
        speed:1000,
    		arrows:true,
    		dots:false,
        fade:true,
        rows:0,
    		infinite:true,
    		slidesToShow:1,
    		slidesToScroll:1,
        lazyload:'ondemand',
        prevArrow:$(".prev"),
        nextArrow:$(".next")
  		});
    });
  }
};


/*------------------------------------------------------------------------------------
    Home slides
-------------------------------------------------------------------------------------*/
$.fn.headerSlideshow = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.slick({
  		autoplay:true,
  		slide:".slide-item",
      fade:true,
  		autoplaySpeed:6000,
      speed:2200,
  		arrows:true,
  		dots:false,
      rows:0,
  		infinite:true,
  		slidesToShow:1,
  		slidesToScroll:1,
      prevArrow:$(".prev"),
      nextArrow:$(".next")
    });
  }
};

$.fn.rolexSlideshow = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.slick({
  		autoplay:true,
  		slide:".slide-item",
      fade:true,
  		autoplaySpeed:4800,
      pauseOnHover:false,
      speed:1000,
  		arrows:false,
  		dots:true,
      rows:0,
  		infinite:true,
  		slidesToShow:1,
  		slidesToScroll:1,
      customPaging : function(slider, i) {
        return '<span class="dot"></span>';
      }
    });
  }
};


/*------------------------------------------------------------------------------------
    Clock bounce
-------------------------------------------------------------------------------------*/
$.fn.clock = function()
{
  var containers = document.querySelectorAll('.seconds-container');

  setInterval(function()
  {
    for (var i = 0; i < containers.length; i++) {
      if (containers[i].angle === undefined) {
        containers[i].angle = 6;
      } else {
        containers[i].angle += 6;
      }
      containers[i].style.webkitTransform = 'rotateZ('+ containers[i].angle +'deg)';
      containers[i].style.transform = 'rotateZ('+ containers[i].angle +'deg)';
    }
  }, 1000);
};


/*------------------------------------------------------------------------------------
    Mobile
-------------------------------------------------------------------------------------*/
$.fn.mobileNav = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $close = $target.find("#close-nav"),
        $body = $("body"),
        $hamburger = $("#hamburger");

    /* hamburger
    ------------------------------*/
		$hamburger.click(function(e)
		{
			e.preventDefault();

			$(this).toggleClass("active");
			$body.toggleClass("opened-menu");

			if( !$target.hasClass("active") )
      {
				$body.addClass("overflowHidden");
        $target.addClass("active");
			}

			return false;
		});

    /* Close nav
    ------------------------------*/
		$close.click(function(e)
		{
			e.preventDefault();

      $hamburger.removeClass("active");
			$body.removeClass("opened-menu").removeClass("overflowHidden");
      $target.removeClass("active");

			return false;
		});
  }
};



/*------------------------------------------------------------------------------------
    Map
-------------------------------------------------------------------------------------*/
$.fn.showMap = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.click(function(e)
    {
      e.preventDefault();

      var $footer = $("#main-footer"),
          $header_height = $("#main-header").height(),
          $map = $('#map'),
          $body = $("html, body");

      if(!$(this).hasClass("active"))
      {
        $(this).addClass("active");

        if(!$body.hasClass("contact-us"))
        {
          $map.animate({
            height: "480px"
          },600).promise().done(function()
          {
            $body.animate({
              scrollTop: $map.offset().top-$header_height
            },600).promise().done(function()
            {
                $map.googleMap();
            });
          });
        }
        else
        {
          $body.animate({
            scrollTop: $map.offset().top-$header_height
          },600);
        }
      }
      else
      {
        $body.animate({
          scrollTop: $map.offset().top-$header_height
        },600);
      }


      return false;
    });
  }
};


/*------------------------------------------------------------------------------------
    Header
-------------------------------------------------------------------------------------*/
$.fn.headerFN = function()
{
  var $target = $(this);

  if($target.length)
  {
    var $window = $(window),
        $banner = $(".page-header"),
        $header_height = $target.height()+80,
        $banner_height = $banner.height(),
        $height = $banner_height-$header_height,
        $pos = $window.scrollTop();

      //Fixed header on scroll
    $window.scroll(function()
    {
       $pos = $window.scrollTop();

       if($pos>=$height)
       {
         $target.addClass("scrolling");
         $('body').addClass("scrolling");
       } else {
         $target.removeClass("scrolling");
         $('body').removeClass("scrolling");
       }
    });
  }
};


/*------------------------------------------------------------------------------------
    Declare FN: Google map - JS only for one MARKER - not multiple locations
-------------------------------------------------------------------------------------*/
$.fn.googleMap = function()
{
  //Get elements and content
  var $map = $(this);

  if($map.length)
  {
    //Defaults
    var $target = $map.find(".marker"),
        $icon_img = null,
        infowindow = null,
        html_content = null,
        snazzymaps_array = null,
        _zoom = 17;

    if($target.length)
    {
      //Check for snazzymaps - ACF field
      if (typeof snazzymaps !== 'undefined')
      {
          snazzymaps_array = snazzymaps;
      }

      var getLat = $target.attr("data-lat"),
          getLng = $target.attr("data-lng"),
          _address = $target.attr("data-address"),
          _icon = $target.attr("data-icon"),
          _content = $target.html(),
          _style = snazzymaps_array;

      if(_address.length)
      {
        // Map options
        var options = {
            center: new google.maps.LatLng(getLat,getLng),
            scrollwheel : false,
            zoom: _zoom,
            styles: _style
          },
          map = new google.maps.Map(document.getElementById('map'), options);

        var geocoderMarker = new google.maps.Geocoder();

        geocoderMarker.geocode( { 'address': _address}, function(results, status)
        {
          if (status === google.maps.GeocoderStatus.OK)
          {
            //Set marker
            //alert(results[0].geometry.location);
            var marker = new google.maps.Marker({
              position: results[0].geometry.location,
              map: map,
              title: _address,
              icon: _icon
            }),
            infowindow = new google.maps.InfoWindow({
              content: _content
            });

            //Info window listener
            if(_content.length)
            {
              marker.addListener('click', function()
              {
                infowindow.open(map, marker);
              });
            }
          }
        });
      } else { alert("No address found"); } //END if(_address.length)
    }//END if($target.length)
  }//END if($map.length)

}; /*END googleMap FN */



/*----------------------------------------------------------------------------------
                    Callback: Render HTML
-----------------------------------------------------------------------------------*/
var renderHtml = function(html)
{
 var result = String(html).replace(/<\!DOCTYPE[^>]*>/i, '')
              .replace(/<(html|head|body|title|script)([\s\>])/gi,'<div id="get-html-$1"$2')
              .replace(/<\/(html|head|body|title|script)\>/gi,'</div>');

 return result;
};


/*----------------------------------------------------------------------------------
                    FN: Load More
-----------------------------------------------------------------------------------*/
$.fn.ajaxLoadMore = function()
{
 //Target pagination
 var $loadMore = $(this);

 if($loadMore.length)
 {
   //Target button
   var $nextBtn = $loadMore.find(".nav-previous > a");

   $nextBtn.click(function( e )
   {
       e.preventDefault();

       //Show spinner
       $loadMore.toggleClass("active");

       //Defaults
       var $page = $(this).attr("href"),
           $last_item = $(".paging-content:last"),
           $next_link = $(this);

         $.ajax({
           url: $page,
           success: function( html, textStatus, jqXHR )
           {
             var $data	= $(renderHtml(html)),
                 $next_href = $data.find(".nav-previous > a").attr("href"),
                 $posts = $data.find(".paging-content").html();

             //Get href for next
             $nextBtn.attr("href", $next_href);

             //Append items
             $last_item.after('<div class="paging-content ajax-paging">'+$posts+'</div>');

             //hide pagination if there is no more pages or toggle spinner
             if($data.find(".nav-previous > a").length)
             {
               $loadMore.toggleClass("active");
             }
             else
             {
               $loadMore.hide().toggleClass("active").addClass("no-more-paging");
             }

           },
           error: function( jqXHR, textStatus, errorThrown )
           {
             //Error
             alert( 'The following error occured: ' + textStatus +' Try to refresh the page or contact us. ' + errorThrown);
           },
           complete: function( jqXHR, textStatus, errorThrown )
           {
             //callbacks
             $(".ajax-post:not(.active)").getAjaxPost();
           }
         });

         return false;

   });//End .click()
 }
};


/* Shortcodes: get post
----------------------------------------------------------------*/
$.fn.getAjaxPost = function()
{
  var $target = $(this);

  if($target.length)
  {
    $target.off('click');
    $target.each(function()
    {
      $(this).on('click',function(e)
    	{
        e.preventDefault();

        var $this = $(this),
            _uri = $this.attr("href"),
            $wrapper = $("#append-post"),
            $header = $("#main-header"),
            _height = $header.height(),
  					$History = window.History,
  					$blog_title = document.title;

    			$History.pushState(null,$blog_title,decodeURIComponent(_uri));

          //Loader
          $header.toggleClass("loader");

    			$("html,body").animate({
    				scrollTop: $wrapper.offset().top-_height
    			},900).promise().done(function()
    			{
            $.ajax({
              url: _uri,
              success: function( html, textStatus, jqXHR )
              {
                var $data	= $(renderHtml(html)),
                    $post_html = $data.find("#append-post").html();

                  $wrapper.html($post_html).addClass("animate-post");
              },
              complete: function( jqXHR, textStatus )
              {
                //Loader
                $header.toggleClass("loader");

                //CALLBACKS
                $(".share-buttons > .anchor-hover").shareThis();
                $("#post-slideshow").postSlideshow();

              },
              error: function( jqXHR, textStatus, errorThrown )
              {
                //Loader
                $header.toggleClass("loader");

                alert(jqXHR+textStatus+errorThrown);
              }
            });
          });

        return false;

      });//end click
    });//end each
  }//if $target
};



})(jQuery); // Fully reference jQuery after this point.
