<?php

/* ACF fields - this is ideally for flexible content */
$location = get_field('map','option'); /*ACF: Google map field - look functions.php */
$icon = get_field('add_icon','option'); /*ACF: Image field array - medium_large */
$content = get_field('info_window_content','option'); /*ACF: Wysiwig field */
$map_style = get_field('map_style','option'); /*ACF: Textarea field with NO FORMATING */

//Defaults
$marker = "";
$html = "";
$snazzy_maps_array = "";

//Infor window content
if( !empty($content) )
{
  $html = $content;
}

//Marker icon
if( !empty($icon) )
{
  $marker = $icon['url'];
}

//Map style array
if( !empty($map_style) )
{
  $snazzy_maps_array = '<script type="text/javascript"> var snazzymaps = '.$map_style.'; </script>';
}
?>

<?php if( !empty($location) ): ?>
<?= $snazzy_maps_array; ?>

<div class="map-wrapper">
  <div class="row">
    <div id="map" class="acf-map"><!--Add css height-->
      <article class="marker"
      data-address="<?= $location['address']; ?>"
      data-lat="<?= $location['lat']; ?>"
      data-lng="<?= $location['lng']; ?>"
      data-icon="<?= $marker; ?>"><?= $html; ?></article>
    </div>
  </div>
</div>

<?php endif; ?>
