<?php

if( have_rows('fx_add_sections') ):

  while ( have_rows('fx_add_sections') ) : the_row();


    /* Custom Section
    ------------------------------------------------------------*/
    if( get_row_layout() == 'custom_section' ): get_template_part('templates/flexible-content/sections-layout/custom_section');

    /* Build your own Section
    ------------------------------------------------------------*/
    //elseif( get_row_layout() == 'my_section' ): get_template_part('templates/flexible-content/sections-layout/custom_section');


    endif;

  endwhile;

else :

//No rows

endif;

?>
