<?php

if( have_rows('fx_add_content') ):


  while ( have_rows('fx_add_content') ) : the_row();

    /* Text Editor
    ------------------------------------------------------------*/
    if( get_row_layout() == 'text_editor' ): get_template_part('templates/flexible-content/elements/text_editor');

    /* Brands
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'brands' ): get_template_part('templates/flexible-content/elements/brands');

    /* Fullscreen image
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'fullscreen_image' ): get_template_part('templates/flexible-content/elements/fullscreen_image');

    /* Call to action
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'call_to_action' ): get_template_part('templates/flexible-content/elements/call_to_action');

    /* Section
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');

    /* Text two columns
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'text_two_columns' ): get_template_part('templates/flexible-content/elements/text_two_columns');

    /* Contact form
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'contact_form' ): get_template_part('templates/flexible-content/elements/contact_form');

    /* Gallery
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'fx_gallery' ): get_template_part('templates/flexible-content/elements/gallery');

    /* Brand Alpha
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'brand_alpha' ): get_template_part('templates/flexible-content/elements/brand_alpha');

    /* Brand Microsite
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'brand_microsite' ): get_template_part('templates/flexible-content/elements/brand_microsite');

    /* Brand Microsite
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'inside_tudor' ): get_template_part('templates/flexible-content/elements/inside_tudor');


    endif;

  endwhile;

else :

// No rows

endif;

?>
