
<?php
//Get page layout
$page_layout = get_field('choose_page_layout');

/** FULLWIDTH **/
if($page_layout == "Fullwidth"):
?>

<div class="page-content">
  <div class="page-wrapper">
    <?php get_template_part('templates/flexible-content/fx-content'); ?>
  </div>
</div>

<?php
/** SECTIONS **/
elseif($page_layout == "Sections"):
?>

<div class="page-content fx-sections">
  <div class="page-wrapper">
  <?php get_template_part('templates/flexible-content/fx-sections'); ?>
  </div>
</div>

<?php
endif;
?>
