<?php

//Fields
$columns = get_sub_field('columns');
$custom_class = get_sub_field('custom_class');
$fluid = get_sub_field('fluid');

//Columns options
$col_one_options = get_sub_field('column_options_one');
$col_two_options = get_sub_field('column_options_two');
$col_three_options = get_sub_field('column_options_three');


$fluid_class = ($fluid[0]=='Yes')? "fluid" : "wrap";

$class = (!empty($custom_class)) ? implode(' ', $custom_class) : " ";

$col1_classses = (!empty($col_one_options)) ? implode(' ', $col_one_options) : "col-md-4";
$col2_classses = (!empty($col_two_options)) ? implode(' ', $col_two_options) : "col-md-4";
$col3_classses = (!empty($col_three_options)) ? implode(' ', $col_three_options) : "col-md-4";



/*================================================================================
  custom_section -> section_fullwidth
=================================================================================*/
if( have_rows('section_fullwidth') ):

echo '<div class="'.$fluid_class.' '.$class.'">';
    echo '<article class="col-xs-12 item">';

      while ( have_rows('section_fullwidth') ) : the_row();

        /* USE ACF FIELD - CLONE to replicate and use existing elements
        ----------------------------------------------------------------------------*/
        if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


        endif;

      endwhile;

    echo '</article>';
echo '</div>';

endif;



/*================================================================================
  custom_section -> section_2_columns
=================================================================================*/

if( have_rows('section_2_columns_one') ):

  echo '<div class="'.$fluid_class.' '.$class.'">';
    echo '<article class="'.$col1_classses.' item">';

      while ( have_rows('section_2_columns_one') ) : the_row();

        /* USE ACF FIELD - CLONE to replicate and use existing elements
        ----------------------------------------------------------------------------*/
        if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


        endif;

      endwhile;

    echo '</article>';


    echo '<article class="'.$col2_classses.' item">';

      while ( have_rows('section_2_columns_two') ) : the_row();

        /* USE ACF FIELD - CLONE to replicate and use existing elements
        ----------------------------------------------------------------------------*/
        if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


        endif;

      endwhile;

    echo '</article>';
  echo '</div>';

endif;



/*================================================================================
  custom_section -> section_3_columns
=================================================================================*/

if( have_rows('section_3_columns_one') ):

  echo '<div class="'.$fluid_class.' '.$class.'">';
      echo '<article class="'.$col1_classses.' item">';

        while ( have_rows('section_3_columns_one') ) : the_row();

          /* USE ACF FIELD - CLONE to replicate and use existing elements
          ----------------------------------------------------------------------------*/
          if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


          endif;

        endwhile;
      echo '</article>';


      echo '<article class="'.$col2_classses.' item">';
        while ( have_rows('section_3_columns_two') ) : the_row();

          /* USE ACF FIELD - CLONE to replicate and use existing elements
          ----------------------------------------------------------------------------*/
          if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


          endif;

        endwhile;
      echo '</article>';


      echo '<article class="'.$col3_classses.' item">';
        while ( have_rows('section_3_columns_three') ) : the_row();

          /* USE ACF FIELD - CLONE to replicate and use existing elements
          ----------------------------------------------------------------------------*/
          if( get_row_layout() == 'section_text_editor' ): get_template_part('templates/flexible-content/elements/section_text_editor');


          endif;

        endwhile;

      echo '</article>';
  echo '</div>';

endif;
