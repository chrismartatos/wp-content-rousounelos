<?php

if( have_rows('fx_aside') ):

  while ( have_rows('fx_aside') ) : the_row();

    /* Text Editor
    ------------------------------------------------------------*/
    if( get_row_layout() == 'text_editor' ): get_template_part('templates/flexible-content/elements/text_editor');


    /* Add fx elements - whatever
    ------------------------------------------------------------*/
    //elseif( get_row_layout() == 'whatever' ): get_template_part('templates/flexible-content/elements/text_editor');

    endif;

  endwhile;

else :

//No rows

endif;

?>
