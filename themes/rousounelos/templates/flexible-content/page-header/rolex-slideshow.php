<?php use Roots\Sage\Titles; ?>

<?php if(!is_404()): ?>
<div id="rolex-slideshow" class="absolute-arrows slideshow-hero">
  <?php
  if( have_rows('slides_repeater') ):
    while ( have_rows('slides_repeater') ) : the_row();
      $img_header = get_sub_field('image_header');
      $bg_header = $img_header['url'];
      $bg_img = (!empty($bg_header))? ' style="background-image:url('.$bg_header.');"' : '' ;

      $page_title = Titles\title();
      $hide_title = get_sub_field('hide_title');
      $header_title = get_sub_field('header_title');
      $link = (!empty(get_sub_field('link')))? get_sub_field('link'): get_sub_field('custom_url');
      $rolex_image = get_sub_field('rolex_image');
      $img_mobile = get_sub_field('image_mobile');
      $main_title = (!empty($header_title))? $header_title : $page_title;
      ?>
      <div class="page-header slide-item rolex-<?= $rolex_image; ?>">

        <div class="rolex-container" onclick="location.href='<?= $link; ?>';">
          <img class="responsive-image image-desktop" src="<?= $bg_header; ?>" alt="<?= $page_title; ?>">
          <img class="responsive-image image-mobile" src="<?= $img_mobile['url']; ?>" alt="<?= $page_title; ?>">

          <section class="wrap-desc rolex-slide-desc">
            <div class="vcenter-outer">
              <div class="vcenter-inner">
                <?php if($link): ?><a href="<?= $link; ?>" class="js-redirect"><?php endif; ?>
                  <?php if($hide_title=="no"): ?>
                  <h2 class="page-title" <?php if($rolex_image=="yes"){ echo 'style="color: #FFF;"'; } ?>><?= $main_title;  ?></h2>
                  <?php endif; ?>
                  <?php
                  if($rolex_image=="no"):
                    echo '<div class="secondary">';
                    the_sub_field('text_editor');
                    echo '</div>';
                  endif;
                  ?>

                  <?php if($rolex_image=="yes"): ?>
                  <?= the_sub_field('button_text'); ?>
                  <?php endif; ?>
                <?php if($link): ?></a><?php endif; ?>
              </div>
            </div>
          </section>

          <?php if($rolex_image=="yes"): ?>

          <section class="wrap-mobile-desc rolex-slide-desc">
            <?php if($link): ?><a href="<?= $link; ?>" class="js-redirect"><?php endif; ?>
              <h2 class="page-title" style="font-family: Helvetica;"><?= $main_title;  ?></h2>
              <?= the_sub_field('button_text'); ?>
            <?php if($link): ?></a><?php endif; ?>
          </section>
          <?php endif; ?>

        </div>
      </div>
      <?php
    endwhile;
    endif;
    ?>
</div>
<?php endif; ?>
