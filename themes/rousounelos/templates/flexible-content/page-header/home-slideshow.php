<?php use Roots\Sage\Titles; ?>

<div id="home-slideshow" class="absolute-arrows slideshow-hero">
  <a class="arrow prev"></a>
  <a class="arrow next"></a>

  <?php
  if( have_rows('slides_repeater') ):
    while ( have_rows('slides_repeater') ) : the_row();

    $img_header = get_sub_field('image_header');
    $bg_header = $img_header['url'];
    $bg_img = (!empty($bg_header))? ' style="background-image:url('.$bg_header.');"' : '' ;

    $page_title = Titles\title();
    $header_title = get_sub_field('header_title');
    $link = get_sub_field('link');
    $main_title = (!empty($header_title))? $header_title : $page_title;

    $header_desc = get_sub_field('header_description');
    $header_secondary = get_sub_field('header_secondary_description');

    $active_desc = (!empty($header_desc))? ' desc-active' : '';
    ?>
    <div class="page-header slide-item">

      <div class="slide-img bg-cover bg-fixed"<?= $bg_img; ?>></div>

      <?php if($header_desc): ?>
      <div class="wrap-desc">
      <?php endif; ?>

      <div class="vcenter-outer">
        <div class="vcenter-inner">

          <div class="container wrap-head">
            <div class="row">

              <div class="col-sm-12 col-md-2"></div>
              <div class="col-sm-12 col-md-8">
                <?php if($hide_page_title !== "Yes"): ?>
                  <?php if($link): ?><a href="<?= $link; ?>" class="js-redirect"><?php endif; ?>
                    <h1 class="page-title<?= $active_desc; ?>"><?= $main_title;  ?></h1>
                  <?php if($link): ?></a><?php endif; ?>
                <?php endif; ?>

                <?php if($header_secondary): ?>
                <div class="secondary"><?= $header_secondary; ?></div>
                <?php endif; ?>
              </div>
              <div class="col-sm-12 col-md-2"></div>

            </div>
          </div><!--END .container-->
        </div><!--END .vcenter-inner-->
      </div><!--END .vcenter-outer-->

      <?php if($header_desc): ?>
      </div><!--END .wrap-desc-->
      <?php endif; ?>
    </div>
    <?php
    endwhile;
  endif;
  ?>
</div>
