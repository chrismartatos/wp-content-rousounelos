<div class="text-editor clearfix">
  <div class="col-sm-12 col-md-2 left-col"></div>
  <div class="col-sm-12 col-md-8">
    <?php the_sub_field('text_editor'); ?>
  </div>
  <div class="col-sm-12 col-md-2 right-col"></div>
</div>
