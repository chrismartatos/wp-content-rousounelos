<?php
$image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
?>
<div class="tudor-page-wrapper">
  <div class="tudor-page-header bg-cover" style="background-image: url(<?= $image_url_full[0]; ?>);">
  </div>

  <div class="tudor-top-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-9 ml-auto mr-auto">
          <hgroup class="tudor-head tc">
            <h1 class="tudor-title tudor-red"><?php the_sub_field('title_1'); ?></h1>
            <h2 class="tudor-subtitle tudor-black"><?php the_sub_field('subtitle_1'); ?></h2>
          </hgroup>
          <div class="tudor-desc tc tudor-black">
            <?php the_sub_field('content'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="tudor-btn-wrap tc">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6 ml-auto mr-auto tc">
          <a href="<?= get_term_link( 131, 'product_cat' ); ?>" class="btn">Discover the Collection</a>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-9 ml-auto mr-auto">
        <hgroup class="tudor-head tc">
          <h1 class="tudor-title tudor-red"><?php the_sub_field('title_2'); ?></h1>
          <h2 class="tudor-subtitle tudor-black"><?php the_sub_field('subtitle_2'); ?></h2>
        </hgroup>
      </div>
    </div>
  </div>

  <div class="tudor-gallery">
    <?php
    if( have_rows('repeater_gallery') ):
      echo '<div class="container">';
      while ( have_rows('repeater_gallery') ) : the_row();
        $image = get_sub_field('image');

        if(!empty($image)):
        ?>
        <img src="<?= $image['url']; ?>" alt="<?php the_title(); ?>"/>
        <?php
        endif;
      endwhile;
      echo '</div>';
    endif;
    ?>

    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-9 ml-auto mr-auto">
          <div class="tudor-desc tudor-black tc">
            <?php the_sub_field('gallery_bottom'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="tudor-video-wrap">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-6">
          <div class="video-desc">
            <hgroup class="tudor-head">
              <h2 class="tudor-title tudor-white"><?php the_sub_field('video_title'); ?></h2>
              <h3 class="tudor-subtitle tudor-white"><?php the_sub_field('video_subtitle'); ?></h3>
            </hgroup>
            <div class="tudor-desc tudor-white">
              <?php the_sub_field('video_content'); ?>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6">
          <div class="video-container">
            <?php the_sub_field('video_embed'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>


  <?php
  if( have_rows('content_repeater') ):
    echo '<div class="tudor-content-repeater">';
    echo '<div class="container">';
    while ( have_rows('content_repeater') ) : the_row();
      $image_pos = get_sub_field('image_position');
      $image_tudor = get_sub_field('image');

      if(!empty($image_tudor)):
      ?>
      <div class="row image-<?= $image_pos; ?>">
        <div class="item col-sm-12 col-md-12 col-lg-6">
          <div class="wrap-content">
            <hgroup class="tudor-head">
              <h2 class="tudor-title tudor-red mb2"><?php the_sub_field('title'); ?></h2>
              <h3 class="tudor-subtitle tudor-black"><?php the_sub_field('subtitle'); ?></h3>
            </hgroup>
            <div class="tudor-desc">
              <?php the_sub_field('description'); ?>
            </div>
          </div>
        </div>
        <div class="item col-sm-12 col-md-12 col-lg-6">
          <?php
          if(!empty($image_tudor)):
          ?>
          <img src="<?= $image_tudor['url']; ?>" alt="<?php the_title(); ?>"/>
          <?php
          endif;
          ?>
        </div>
      </div>
      <?php
      endif;
    endwhile;
    echo '</div>';
    echo '</div>';
  endif;
  ?>


  <div class="tudor-btn-wrap tc">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-6 ml-auto mr-auto tc">
          <a href="<?= get_term_link( 131, 'product_cat' ); ?>" class="btn">Discover the Collection</a>
        </div>
      </div>
    </div>
  </div>

  <?php
  $bottom_image = get_sub_field('bottom_image');
  ?>
  <div class="tudor-image-bottom bg-cover" style="background-image: url(<?= $bottom_image['url']; ?>);">
    <div class="tudor-subtitle tc tudor-white"><?php the_sub_field('bottom_title'); ?></div>
  </div>


</div><!-- .tudor-page-wrapper -->
