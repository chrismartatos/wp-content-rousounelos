<?php
$image = get_sub_field('image');
?>

<figure class="fullscreen-image">
  <img src="<?= $image['url']; ?>" alt="<?php the_title(); ?>">
</figure>
