<div class="clearfix">
<?php if( have_rows('gallery') ): ?>
 	<?php
  $counter = 1;
  $arrowprev = 1;
  $arrownext = 1;
  ?>

  <?php while( have_rows('gallery') ): the_row(); ?>
    <div class="brand-alpha gallery-<?= $counter++; ?>">

      <?php
      $images = get_sub_field('gallery');
      $speed = get_sub_field('speed');
      $link = get_sub_field('link');
      $hide = (!empty($link))? ' style="display:none;"' : '';
      ?>

      <?php if( $images ):   ?>

        <div class="brand-slideshow fullscreen-image absolute-arrows" data-speed="<?= $speed; ?>">
          <a class="arrow prev prev-<?= $arrowprev++; ?>"<?= $hide; ?>></a>
          <a class="arrow next next-<?= $arrownext++; ?>"<?= $hide; ?>></a>
          <?php foreach( $images as $image ): ?>
          <div class="slide-item">
	          <?php if(!empty($link)): ?>
	          	<a href="<?= $link; ?>" target="_blank" title="<?php the_title(); ?>">
	          <?php endif; ?>
			  <img data-lazy="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
			  <?php if(!empty($link)): ?>
	          	</a>
	          <?php endif; ?>
          </div>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
  <?php endwhile; ?>
<?php endif; ?>
</div>
