<?php
$images = get_sub_field('gallery');
$speed = get_sub_field('speed');
$fade = get_sub_field('fade');

if( $images ): ?>
  <div class="fx-gallery fullscreen-image absolute-arrows"  data-speed="<?= $speed; ?>" data-fade="<?= $fade; ?>">
    <a class="arrow prev"></a>
    <a class="arrow next"></a>
    <?php foreach( $images as $image ): ?>
    <div class="slide-item">
      <img data-lazy="<?= $image['url']; ?>" alt="<?php the_title(); ?> <?= $image['alt']; ?>">
    </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>
