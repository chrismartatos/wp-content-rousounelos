<?php $title = get_sub_field('title'); ?>
<div class="contact-form-wrap clearfix">
  <fieldset class="item">
    <?php if($title):?>
      <h2><?= $title; ?></h2>
    <?php endif; ?>
    <?= do_shortcode('[contact-form-7 id="76"]') ?>
  </fieldset>
</div>
