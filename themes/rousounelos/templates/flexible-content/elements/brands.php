<?php

$brands_args = array(
  'post_type' => 'brand',
  'post_status' => 'publish',
  'orderby' => 'date',
  'order' => 'ASC',
  'post_per_page' => -1,
  'nopaging' => true
);

//Query
$brands_query = new WP_Query( $brands_args );
?>

<div class="clearfix brands-grid">
 <?php
 if($brands_query->have_posts()) :
   while ($brands_query->have_posts()) : $brands_query->the_post();
      get_template_part('templates/brands/entry-post');
   endwhile;

   wp_reset_postdata();
 endif;
 ?>
</div>
