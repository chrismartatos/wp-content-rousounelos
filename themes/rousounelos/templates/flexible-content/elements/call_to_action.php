<?php
if( have_rows('add_action') ):
  echo '<div class="call-to-action">';
  while ( have_rows('add_action') ) : the_row();
    $title = get_sub_field('title');
    $link = get_sub_field('link');
    $open = get_sub_field('open_on_new_window');

    $target = ($open=="Yes")? ' target="_blank"' : '';
    ?>
    <a class="item js-redirect" href="<?= $link; ?>"<?= $target; ?>>
      <h2><?= $title; ?></h2>
    </a>
    <?
  endwhile;
  echo '</div>';
endif;
?>
