<div class="text-editor-wrap clearfix">

  <div class="text-editor-one col-xs-12 col-sm-6">
    <article class="item">
      <?php the_sub_field('text_editor_one'); ?>
    </article>
  </div>

  <div class="text-editor-two col-xs-12 col-sm-6">
    <article class="item">
      <?php the_sub_field('text_editor_two'); ?>
    </article>
  </div>

</div>
