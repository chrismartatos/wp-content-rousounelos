<script type="text/javascript">

//plain js - loader script
var js_target_preloader = document.getElementById("preloader");
var js_target_body = document.getElementsByTagName("BODY")[0];

js_target_body.classList.add('loading');

//Storage
if (typeof(Storage) !== "undefined")
{
	if(sessionStorage.getItem('rousounelos') !== 'site_loaded')
	{
		//Start session - first session
		sessionStorage.setItem('rousounelos', 'site_loaded');

		setTimeout(function()
		{
			js_target_body.classList.add('content-bounce');
		},2000);
	}
} else {
	//do nothing
}

setTimeout(function()
{
	js_target_body.classList.add('clock-loaded');
},2800);
</script>

<?php
$address = get_field('address','option');
$preloader_logo = get_field('preloader_logo','option');
?>

<div id="preloader">
	<div class="vcenter-outer">
		<div class="vcenter-inner">

      <div class="clock-wrap">
				<span class="triangle"></span>
				<span class="circle circle-1"></span>
				<span class="circle circle-2"></span>
				<span class="square square-1"></span>
				<span class="circle circle-3"></span>
				<span class="circle circle-4"></span>
				<span class="square square-2"></span>
				<span class="circle circle-5"></span>
				<span class="circle circle-6"></span>
				<span class="square square-3"></span>
				<span class="circle circle-7"></span>
				<span class="circle circle-8"></span>
			</div>

		</div>
	</div>
</div>
