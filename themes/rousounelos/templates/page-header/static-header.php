<?php
  use Roots\Sage\Titles;

  global $post;

  $_terms = wp_get_post_terms( $post->ID, 'brand_category', array("fields" => "all"));
  $category = (!empty($_terms)) ? $_terms[0]->slug : "" ;

  $image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
  $feat_img = $image_url_full[0];

  $img_header = get_field('image_header');
  $bg_header = (!empty($img_header))?$img_header['url']: array();
  $bg_img = (!empty($bg_header))? ' style="background-image:url('.$bg_header.');"' : '' ;

  $hide_page_title = get_field('hide_page_title');
  $page_title = Titles\title();
  $header_title = get_field('header_title');
  $main_title = (!empty($header_title))? $header_title : $page_title;

  $header_desc = get_field('header_description');
  $header_secondary = get_field('header_secondary_description');

  $active_desc = (!empty($header_desc))? ' desc-active' : '';
  $hide = ($category == "microsite")? ' style="display:none;"' : '';

  if(!is_404()):
  	/*BRANDS MICROSITE HEADER*/
    if(($category == "microsite")):
      get_template_part('templates/page-header/header-mobile');
    else:
?>
  <div class="page-header bg-cover bg-fixed <?= $category; ?>"<?= $bg_img; ?>>

    <?php if($header_desc): ?>
    <div class="wrap-desc">
    <?php endif; ?>

    <div class="vcenter-outer">
      <div class="vcenter-inner">

        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-1 col-lg-2 left-item"></div>

            <div class="col-sm-12 col-md-10 col-lg-8 middle">
              <?php if(is_singular('brand')): ?>
                <?php
  	            $svg_code = get_field('svg_code');

                  if(!empty($svg_code)):
                  ?>
                  <div class="brand-svg"<?php echo $hide; ?>>
                  <?php echo $svg_code; ?>
                  </div>
                <?php else: ?>
                <img class="brand-img" src="<?= $feat_img; ?>" alt="<?php the_title(); ?>">
                <?php endif; ?>
              <?php endif; ?>

              <?php
              if(!is_singular('post')):
                if($hide_page_title !== "Yes"): ?>
                <h1 class="page-title<?= $active_desc; ?>"><?= $main_title;  ?></h1>
                <?php
                endif;
              endif; ?>

              <?php if($header_desc): ?>
              <div class="desc"><p><?= $header_desc; ?></p></div>
              <?php endif; ?>

              <?php if($header_secondary): ?>
              <div class="secondary"><?= $header_secondary; ?></div>
              <?php endif; ?>
            </div>

            <div class="col-sm-12 col-md-1 col-lg-2 right-item"></div>
          </div>
        </div><!--END .container-->

      </div><!--END .vcenter-inner-->
    </div><!--END .vcenter-outer-->

    <?php if($header_desc): ?>
    </div><!--END .wrap-desc-->
    <?php endif; ?>
  </div>

  <?php endif; ?>
<?php endif; ?>
