<?php
  use Roots\Sage\Titles;

  $_terms = wp_get_post_terms( $post->ID, 'brand_category', array("fields" => "all"));
  $category = (!empty($_terms)) ? $_terms[0]->slug : "" ;

  $image_url_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true);
  $feat_img = $image_url_full[0];

  $img_header = get_field('image_header');
  $bg_header = $img_header['url'];
  $bg_img = (!empty($bg_header))? ' style="background-image:url('.$bg_header.');"' : '' ;

  $header_desc = get_field('header_description');
  $header_secondary = get_field('header_secondary_description');

  $hide = ($category == "microsite")? ' style="display:none;"' : '';

?>
  <div id="microsite-header" class="page-header bg-cover bg-fixed
  <?php if(wp_is_mobile()): echo ' microsite'; else: echo $category; endif; ?>"
    <?php if($category !== "microsite"): echo $bg_img; else: echo ''; endif; ?>>

    <div class="wrap-desc">

    <div class="vcenter-outer">
      <div class="vcenter-inner">

        <div class="container">
          <div class="row">

            <div class="col-sm-12 col-md-1 col-lg-2 left-item"></div>
            <div class="col-sm-12 col-md-10 col-lg-8 middle">
              <?php if(is_singular('brand')): ?>
                <?php
  	            $svg_code = get_field('svg_code');

                  if(!empty($svg_code)):
                  ?>
                  <div class="brand-svg"<?php echo $hide; ?>><?= $svg_code; ?></div>
                  <?php else: ?>
                  <img class="brand-img" src="<?= $feat_img; ?>" alt="<?php the_title(); ?>">
                  <?php endif; ?>
                <?php endif; ?>

              <?php if($category !== "microsite"): ?>

                <?php if($header_desc): ?>
                <div class="desc"><p><?= $header_desc; ?></p></div>
                <?php endif; ?>

                <?php if($header_secondary): ?>
                <div class="secondary"><?= $header_secondary; ?></div>
                <?php endif; ?>

              <?php endif; ?>

            </div>
            <div class="col-sm-12 col-md-1 col-lg-2 right-item"></div>

          </div>
        </div><!--END .container-->

      </div><!--END .vcenter-inner-->
    </div><!--END .vcenter-outer-->

    </div><!--END .wrap-desc-->
  </div>
