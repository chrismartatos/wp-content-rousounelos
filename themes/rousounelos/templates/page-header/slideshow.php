<?php
if( have_rows('slideshow') ):
  while ( have_rows('slideshow') ) : the_row();

    /* Custom Section
    ------------------------------------------------------------*/
    if( get_row_layout() == 'slide' ): get_template_part('templates/flexible-content/page-header/home-slideshow');

    /* Build your own Section
    ------------------------------------------------------------*/
    elseif( get_row_layout() == 'rolex_slideshow' ): get_template_part('templates/flexible-content/page-header/rolex-slideshow');

    endif;

  endwhile;

  else :
    //no rows
endif;
?>
