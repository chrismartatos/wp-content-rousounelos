<?php
  use Roots\Sage\Titles;
?>
  <div class="singular-header page-header">

    <div class="wrap-desc">

    <div class="vcenter-outer">
      <div class="vcenter-inner">

        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-1 col-lg-2 left-item"></div>

            <div class="col-sm-12 col-md-10 col-lg-8 middle"></div>

            <div class="col-sm-12 col-md-1 col-lg-2 right-item"></div>
          </div>
        </div><!--END .container-->

      </div><!--END .vcenter-inner-->
    </div><!--END .vcenter-outer-->

    </div><!--END .wrap-desc-->

  </div>
