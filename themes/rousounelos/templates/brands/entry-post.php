<?php
$get_post = get_post(get_the_id());
$custom_redirect = get_field('custom_redirect');
$href = (!empty($custom_redirect))? $custom_redirect : get_permalink(); 
$image_full = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', false);

$brand_black = get_field('logo_black');
$brand_white = get_field('logo_white');
$svg_code = get_field('svg_code');
?>

<div class="brands-item">
  <a href="<?= $href; ?>" class="<?= $get_post->post_name; ?> item js-redirect">
    <div class="vcenter-outer">
      <div class="vcenter-inner">
        <?php
        if(!empty($svg_code)):
          echo $svg_code;
        else:
          $brand_img = $image_full[0];
        ?>
        <img src="<?= $brand_img; ?>" alt="<?php bloginfo('name').' '.the_title(); ?>">
        <?php
        endif;
        ?>
      </div>
    </div>
  </a>
</div>
