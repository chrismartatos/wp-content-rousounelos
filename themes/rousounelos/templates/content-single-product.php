<div class="container">
  <div class="single-product-wrapper">
    <?php
    while (have_posts()) : the_post();
      woocommerce_breadcrumb();
      the_content();
    endwhile;
    ?>
  </div>
</div>

<section id="product-inquiries" class="contact-form-wrap clearfix">
  <h4 class="title">Product Inquiries</h4>

  <fieldset class="item">
    <?= do_shortcode('[contact-form-7 id="76"]') ?>
  </fieldset>
</section>
