<?php
$logo_white = get_field('website_logo','option');
$logo_black = get_field('website_logo_black','option');

$logo_icon_white = get_field('icon_white','option');
$logo_icon_black = get_field('icon_black','option');

/**** MOBILE NAV ****/
get_template_part('templates/mobile/nav');


get_template_part('templates/rolex-clock-header');
?>

<a href="<?= get_permalink(94); ?>" class="patek-brand-logo fixed-pos"></a>

<header id="main-header">

  <div class="css-loader"></div>

  <a class="site-logo black js-redirect" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
    <img src="<?= $logo_icon_black['url']; ?>" alt="<?php bloginfo('name'); ?>" class="logo-icon">
    <img src="<?= $logo_black['url']; ?>" alt="<?php bloginfo('name'); ?>">
  </a>
  <a class="site-logo white js-redirect" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
    <img src="<?= $logo_icon_white['url']; ?>" alt="<?php bloginfo('name'); ?>" class="logo-icon">
    <img src="<?= $logo_white['url']; ?>" alt="<?php bloginfo('name'); ?>">
  </a>

  <nav id="rolex-navigation" class="nav-primary">
    <?php
    if (has_nav_menu('rolex_navigation')) :
      wp_nav_menu(['theme_location' => 'rolex_navigation', 'menu_class' => 'nav']);
    endif;
    ?>
  </nav>

  <!--nav id="main-navigation" class="nav-primary">
    <?php
    if (has_nav_menu('primary_navigation')) :
      //wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
    endif;
    ?>
  </nav-->


  <a id="hamburger" href="#">
    <span class="line-1 line"></span>
    <span class="line-2 line"></span>
  </a>
</header>
