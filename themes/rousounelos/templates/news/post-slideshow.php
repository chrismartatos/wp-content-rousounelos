<?php
$images = get_field('post_slideshow');

if( $images ): ?>
<div class="col-sm-12 col-md-6">
  <div id="post-slideshow" class="clearfix img-wrap absolute-arrows">
    <a class="arrow prev"></a>
    <a class="arrow next"></a>
    <?php foreach( $images as $image ): ?>
    <figure class="slide-item">
      <img data-lazy="<?= $image['url']; ?>" alt="<?php the_title(); ?>">
    </figure>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>
