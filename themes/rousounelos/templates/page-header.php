<?php
  use Roots\Sage\Titles;

  global $wp_query;

  //SLIEDESHOW
  $add_lideshow = get_field('add_slideshow');

  if($add_lideshow == "Yes"):

    get_template_part('templates/page-header/slideshow');

  else:

    if(is_home())
    {
      get_template_part('templates/page-header/static-header-news');
    }
    elseif(is_singular('product'))
    {
      get_template_part('templates/page-header/static-header-product');
    }
    elseif(is_shop())
    {
      get_template_part('templates/page-header/static-header-product');
    }
    elseif(is_product_category() || is_product_category())
    {
      get_template_part('templates/page-header/static-header-product');
    } else {
      get_template_part('templates/page-header/static-header');
    }

  endif;
?>
