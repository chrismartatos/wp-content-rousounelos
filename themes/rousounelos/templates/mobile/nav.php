<?php
$address = get_field('address','option');
$mobile_logo = get_field('mobile_logo','option');
?>
<div id="mobile-nav-container">
	<a id="close-nav" href="#"></a>

	<div class="vcenter-outer">
		<div class="vcenter-inner">
			<a class="js-redirect mobile-logo" href="<?= get_home_url(); ?>">
				<img src="<?= $mobile_logo['url'];?>" alt="<?php bloginfo('name'); ?>">
			</a>
			<nav id="mobile-menu">
        <?php
        if (has_nav_menu('mobile_navigation')) :
          wp_nav_menu(['theme_location' => 'mobile_navigation']);
        endif;
        ?>
			</nav>
			<div class="mobile-address">
				<?= $address; ?>
			</div>
			<?php get_template_part('templates/rolex-clock-mobile'); ?>

			<a href="<?= get_permalink(94); ?>" class="patek-brand-logo"></a>
		</div>
	</div>

</div>
