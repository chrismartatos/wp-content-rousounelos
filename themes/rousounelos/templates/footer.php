
<?php
//Footer ACF
$address = get_field('address','option');
$copyrights = get_field('copyrights','option');
$map_title = get_field('map_title','option');

//Google map
get_template_part('templates/map/map');
?>
<footer id="main-footer" class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="footer-info">
        <div class="address">
          <?= $address; ?>
          <?php if(!empty($map_title)): ?>
            <p><a href="#" id="map-init"><?= $map_title; ?></a></p>
          <?php endif; ?>
        </div>

        <nav class="footer-nav cul">
          <?php
          if (has_nav_menu('footer_navigation')):
            wp_nav_menu(['theme_location' => 'footer_navigation']);
          endif;
          ?>
        </nav>

        <address class="copyrights"><?= $copyrights; ?></address>
      </div>
    </div>
  </div>
</footer>
