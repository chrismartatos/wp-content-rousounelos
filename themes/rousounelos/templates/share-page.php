<?php $uri = get_home_url(); ?>
<nav class="share-buttons share-page">
  <p><a href="#" class="anchor-hover">Share this page <span class="plus"><img src="<?= $uri; ?>/wp-content/uploads/2017/03/ROUSOUNELOS-CONTACT-US_03.png" alt="share" width="23" height="26"></span></a></p>
  <ul class="share-social">
    <li><a target="_blank" href="http://www.facebook.com/share.php?u=<?= $uri; ?><?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" class="fa fa-facebook social-fb"></a></li>
    <li><a target="_blank" href="http://twitter.com/share?text=<?php ?>&amp;url=<?= $uri; ?><?php the_permalink(); ?>" class="fa fa-twitter social-tw"></a></li>
    <li><a target="_blank" href="mailto:example@email.com?subject=<?php the_title(); ?>&amp;body=<?= $uri; ?><?php the_permalink(); ?>" class="fa fa-envelope social-em"></a></li>
  </ul>
</nav>
