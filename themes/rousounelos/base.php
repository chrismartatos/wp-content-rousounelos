<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;
?>

<!doctype html>
<html <?php language_attributes(); ?>>

  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>

  <?php /**** PRELOADER ****/
  get_template_part('templates/preloader');
  ?>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <?php
      do_action('get_header');
      get_template_part('templates/header');

      //Password required
      if( ! post_password_required() ):
        get_template_part('templates/page', 'header');
      endif;
    ?>
    <div class="main-content" role="document">
      <main class="main">
        <?php if( ! post_password_required() ): ?>

          <?php include Wrapper\template_path(); ?>

        <?php elseif( post_password_required() ): ?>
          <!-- PASSWORD PROTECTED AREA --->
          <div class="password-protected">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-md-4 offset-md-4">
                  <?php the_content(); ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>

      </main><!-- /.main -->
    </div><!-- /.wrap -->

    <?php
    if( ! post_password_required() ):
      //Rolex clock JS
      get_template_part('templates/rolex-clock-js');
    endif;
    ?>

    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
